import { Routes } from "react-router-dom"
import Header from "./components/Header/Header"

function App() {
  return (
    <>
      <Header />
      <Routes></Routes>
    </>
  )
}

export default App
