import React, { useState } from "react"
import { FormControl, InputLabel, Select, MenuItem } from "@mui/material"

const CurrancySelect = () => {
  const [cur, selectCur] = useState("usd")
  const handleChange = (event) => {
    selectCur(event.target.value)
  }
  return (
    <FormControl fullWidth sx={{ width: "85px" }}>
      <InputLabel id="lang"></InputLabel>
      <Select
        sx={{ border: "0" }}
        labelId="lang"
        id="lang"
        value={cur}
        label="lang"
        onChange={handleChange}
      >
        <MenuItem value={"usd"}>USD</MenuItem>
        <MenuItem value={"uah"}>UAH</MenuItem>
      </Select>
    </FormControl>
  )
}
export default CurrancySelect
