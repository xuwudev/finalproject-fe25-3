import React from "react"
import LanguageSelect from "./LanguageSelect"
import CurrancySelect from "./CurrancySelect"
import { AppBar, Toolbar, Stack, Button } from "@mui/material"
import svg from "./svg.svg"
import cart from "./cart.svg"
import search from "./search.svg"
const Header = ({ amount, selectedCurrancy, carteditems }) => {
  return (
    <header className="header">
      <div className="header-wrapper">
        <AppBar position="static">
          <Toolbar style={{ backgroundColor: "white" }}>
            <div className="selector-wrapper">
              <LanguageSelect />
              <CurrancySelect />
            </div>
            <div className="right-side">
              <Stack direction="row" spacing={2} alignItems="flex-end">
                <Button color="inherit">
                  <img src={svg} alt="" />
                  my prfile
                </Button>
                <Button color="inherit">
                  <img src={cart} alt="" />
                  <div className="items-carted">{carteditems}</div>
                </Button>
                <Button color="inherit">Items</Button>
                <Button color="inherit">
                  {selectedCurrancy}
                  {amount}
                </Button>
                <Button>
                  <img src={search} alt="" />
                </Button>
              </Stack>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    </header>
  )
}
export default Header
