import React, { useState } from "react"
import { FormControl, InputLabel, Select, MenuItem } from "@mui/material"

const LanguageSelect = () => {
  const [lang, selectLang] = useState("en")
  const handleChange = (event) => {
    selectLang(event.target.value)
  }
  return (
    <FormControl fullWidth sx={{ width: "70px" }}>
      <InputLabel id="lang"></InputLabel>
      <Select
        labelId="lang"
        id="lang"
        value={lang}
        label="lang"
        onChange={handleChange}
      >
        <MenuItem value={"en"}>En</MenuItem>
        <MenuItem value={"ru"}>Ru</MenuItem>
        <MenuItem value={"ua"}>Ua</MenuItem>
      </Select>
    </FormControl>
  )
}
export default LanguageSelect
